/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/20 15:37:35 by dbonilla          #+#    #+#             */
/*   Updated: 2022/07/20 15:37:37 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*suma(char *str, int i)
{
	if ((str[i + 1] >= 97) && (str[i + 1] <= 122))
	{	
		str[i + 1] = str[i + 1] - 32;
	}
	return (str);
}

char	*ft_strlowcase(char *str)
{
	int	i	;

	i = 0;
	while (str[i] != '\0' )
	{
		if (str[i] >= 65 && str[i] <= 90)
		{
			str[i] = str[i] + 32;
		}
		i++;
	}
	return (str);
}

char	*ft_strcapitalize(char *str)
{
	int	i ;

	i = 0;
	str = ft_strlowcase (str);
	while (str[i] != '\0')
	{
		if ((str[i] < 48) && (str[i] > 122))
		{		
			str = suma(str, i);
		}
		if (i == 0)
		{
						str = suma(str, i - 1);
		}
		if ((str[i] >= 91 && str[i] <= 96) || (str[i] >= 58 && str[i] <= 64))
		{
				str = suma(str, i);
		}						
		i++;
	}
	return (str);
}
