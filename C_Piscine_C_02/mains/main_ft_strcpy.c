#include<stdio.h>

char	*ft_strcpy(char *destinn, char *source);

int	main(void)
{
	char source[] = "Source string";
	char destin[] = "Destination string";
	char *dest;
	printf("BEFORE\n\tsrc: %s\n\tdes: %s\n", source, destin);
	dest = ft_strcpy(destin, source);
	printf("AFTER\n\tsrc: %s\n", dest);
	return (0);
}
