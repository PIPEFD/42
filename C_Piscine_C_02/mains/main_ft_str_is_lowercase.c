#include<stdio.h>

int		main(void)
{
	char numeric[] = "hola";
	char *p_num;
	p_num = numeric;

	char special[] = "Hola";
	char *p_spe;
	p_spe = special;

	char empty[] = "";
	char *p_emp;
	p_emp = empty;

	printf("-----\n1 = String contains only numerical chars\n0 = String doesn't contain only numerical chars\n\n");
	//printf("%s = %d\n", numeric, ft_str_is_lowercase(p_num));
	printf("%s = %d\n", special, ft_str_is_lowercase(p_spe));
	printf("Empty = %d\n-----\n", ft_str_is_lowercase(p_emp));

	return (0);
}