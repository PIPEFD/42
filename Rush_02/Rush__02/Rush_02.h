/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Rush_02.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/17 15:41:04 by dbonilla          #+#    #+#             */
/*   Updated: 2022/07/17 15:41:06 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
# ifndef RUSH_02_H
# define RUSH_02_H

# include "stdio.h"
# include "fcntl.h"
# include "unistd.h"
# include "stdlib.h"

void	ft_putstr(char *str);
void	findzero(char *str, char **listnum, char **listname);
void	start(char *input);
void	print_three_number(char *str, char **listnum, char **listname);
void	selectpow(char *str, char **listnum, char **listname);
int		errordic(int fila, int a);
void	freememory(char **matnum, char **matname, int index);
void	findpow(char *dest, char **listnum, char **listname, int a);

#endif