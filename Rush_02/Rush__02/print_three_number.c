/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_three_number.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emelende <emelende@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/16 16:51:27 by ubegona           #+#    #+#             */
/*   Updated: 2022/07/17 18:12:18 by emelende         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "Rush_02.h"


void	ft_putstr(char *str)
{
	char	c;
	int		i;

	i = 0;
	while (str[i] != '\0')
	{
		c = str[i];
		write(1, &c, 1);
		i++;
	}
	write(1, " ", 1);
}

int	singlenum(int a, int b, char *str, char **listnum)
{
	int	j;

	j = 0;
	if (str[b - 1] == '1' && str[b] == '0' && str[b + 1] != '0')
	{
		while (listnum[j])
		{
			if (listnum[j][0] == '1' && listnum[j][1] == '0')
			{	
				if (listnum[j][2] != '0')
					return (j);
			}
			j++;
		}
	}
	while (listnum[j])
	{
		if (listnum[j][a] == str[b] && str[b] != '0')
			return (j);
		j++;
	}
	return (200);
}

void	hundred(char *str, char **listnum, char **listname)
{
	int	j;

	j = 0;
	if (str[0] != '0')
	{
		while (listnum[j])
		{
			if (listnum[j][0] == '1' && listnum[j][2] == '0')
			{
				if (listnum[j][3] != '0')
				{
					ft_putstr(listname[j]);
					break ;
				}
			}
			j++;
		}
	}
}

void	tens(char *str, char **listnum, char **listname)
{
	int	j;

	j = 0;
	if (str[1] != '0')
	{
		while (listnum[j])
		{
			if (listnum[j][0] == str[1] && listnum[j][1] == '0')
			{
				ft_putstr(listname[j]);
				break ;
			}
			j++;
		}
	}
}

void	print_three_number(char *str, char **listnum, char **listname)
{
	int	j;

	j = singlenum(0, 0, str, listnum);
	if (j != 200)
		ft_putstr(listname[j]);
	hundred(str, listnum, listname);
	if (str[1] == '1')
	{
		j = singlenum(1, 2, str, listnum);
		if (j != 200)
			ft_putstr(listname[j]);
	}
	else
	{
		tens(str, listnum, listname);
		j = singlenum(0, 2, str, listnum);
		if (j != 200)
			ft_putstr(listname[j]);
	}
}
