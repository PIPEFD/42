/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ubegona <ubegona@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/17 19:19:43 by ubegona           #+#    #+#             */
/*   Updated: 2022/07/17 19:54:25 by ubegona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "Rush_02.h"



int	ft_argv_three(char *argv[], int i, int a)
{
	while (argv[2][i] != '\0')
	{
		if (!(argv[1][i] >= '0' && argv[1][i] <= '9'))
			return (1);
		i++;
		a = 2;
	}
	return (a);
}

int	check_errors(int argc, char *argv[])
{
	int	i;
	int	a;

	i = 0;
	a = 0;
	if (!(argc == 2 || argc == 3))
		return (1);
	if (argc == 2)
	{
		while (argv[1][i] != '\0')
		{
			if (!(argv[1][i] >= '0' && argv[1][i] <= '9'))
				return (1);
			i++;
			a = 1;
		}
	}
	else
	{
		a = ft_argv_three(argv, i, a);
	}
	return (0);
}

int	main(int argc, char **argv)
{
	int	a;

	a = check_errors(argc, argv);
	if (a == 1)
	{
		write(2, "Error\n", 6);
		return (0);
	}
	else
	{
		start(argv[a + 1]);
	}
}
