/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   selectpow.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ubegona <ubegona@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/17 10:01:18 by ubegona           #+#    #+#             */
/*   Updated: 2022/07/17 20:08:01 by ubegona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Rush_02.h"


int	ft_divide_three(int len)
{
	int		i;

	i = 0;
	if (len % 3 == 0)
		i = 0;
	if (len % 3 == 2)
		i = 1;
	if (len % 3 == 1)
		i = 2;
	return (i);
}

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

void	selectpow(char *str, char **listnum, char **listname)
{
	int		len;
	int		j;
	int		i;
	int		a;
	char	dest[3];

	j = 0;
	dest[0] = '0';
	dest[1] = '0';
	len = ft_strlen(str);
	i = ft_divide_three(len);
	while (j < len)
	{
		a = 0;
		while (i < 3)
		{
			dest[i] = str[j + a++];
			i++;
		}
		print_three_number(dest, listnum, listname);
		findpow(dest, listnum, listname, len - j - a);
		i = 0;
		j = j + a;
	}
	findzero(str, listnum, listname);
}

void	findpow(char *dest, char **listnum, char **listname, int a)
{
	int	j;

	j = 0;
	if (dest[0] != '0' || dest[1] != '0' || dest[2] != '0')
	{
		while (listnum[j])
		{
			if (listnum[j][0] == '1' && listnum[j][a] == '0')
			{
				if (listnum[j][a + 1] != '0')
				{
					ft_putstr(listname[j]);
					break ;
				}
			}
			j++;
		}
	}
}
