/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   findzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ubegona <ubegona@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/17 19:38:53 by ubegona           #+#    #+#             */
/*   Updated: 2022/07/17 20:10:44 by ubegona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "Rush_02.h"



void	findzero(char *str, char **listnum, char **listname)
{
	int	i;
	int	j;
	int	a;

	i = 0;
	j = 0;
	a = 0;
	while (str[i])
	{
		if (str[i] == '0')
			j++;
		i++;
	}
	if (i == j)
	{
		while (listnum[a])
		{
			if (listnum[a][0] == '0' && listnum[a][1] == '\0')
			{
				ft_putstr(listname[a]);
				break ;
			}	
			a++;
		}
	}
}
