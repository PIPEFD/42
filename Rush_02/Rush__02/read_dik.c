/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_dik.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ubegona <ubegona@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/16 10:41:44 by ubegona           #+#    #+#             */
/*   Updated: 2022/07/17 20:23:28 by ubegona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "Rush_02.h"



void	read_dik(char **listnum, char **listname, int fila, int a)
{
	int		p;
	char	c;
	int		i;

	p = open("numbers.dict.txt", O_RDONLY);
	while (a < fila)
	{
		read(p, &c, 1);
		i = -1;
		while (c >= '0' && c <= '9')
		{
			listnum[a][++i] = c;
			read(p, &c, 1);
		}
		while (c == ' ' || c == ':')
			read(p, &c, 1);
		i = -1;
		while (c >= 32 && c <= 127)
		{
			listname[a][++i] = c;
			read(p, &c, 1);
		}
		a++;
	}
	close (p);
}

int	filas(void)
{
	int		p;
	char	c;
	int		i;

	i = 0;
	p = open("numbers.dict.txt", O_RDONLY);
	if (p == -1)
	{
		write(1, "dict error\n", 11);
	}
	read(p, &c, 1);
	while (read(p, &c, 1))
	{
		if (c == '\n')
		{
			i++;
		}
	}
	close(p);
	return (i);
}

int	columnas(void)
{
	int		p;
	char	c;
	int		max;
	int		i;

	i = 0;
	max = 0;
	p = open("numbers.dict.txt", O_RDONLY);
	if (p == -1)
	{
		write(1, "dict error\n", 11);
	}
	read(p, &c, 1);
	while (read(p, &c, 1))
	{
		if (c == '\n' && i > max)
		{
			max = i;
		}
		i++;
	}
	close(p);
	return (max);
}

void	start(char *input)
{
	int		index;
	char	**matnum;
	char	**matname;
	int		i;
	int		j;

	i = filas();
	j = errordic(i, 0);
	if (j == 0)
		write(1, "Error de dic", 12);
	else
	{
		j = columnas();
		matnum = malloc(i * sizeof(char *));
		index = -1;
		while (++index < i)
			matnum[index] = malloc(j * sizeof(char));
		matname = malloc(i * sizeof(char *));
		index = -1;
		while (++index < i)
			matname[index] = malloc(j * sizeof(char));
		read_dik(matnum, matname, i, 0);
		selectpow(input, matnum, matname);
	}
}
