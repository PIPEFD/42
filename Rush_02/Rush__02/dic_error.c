/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dic_error.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ubegona <ubegona@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/17 16:53:06 by ubegona           #+#    #+#             */
/*   Updated: 2022/07/17 18:18:38 by ubegona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "Rush_02.h"



int	errordic(int fila, int a)
{
	int		p;
	char	c;

	p = open("numbers.dict.txt", O_RDONLY);
	while (a < fila)
	{
		read(p, &c, 1);
		while (c >= '0' && c <= '9' && read(p, &c, 1))
			;
		if (c != ' ' && c != ':')
			return (0);
		while (c == ' ' && read(p, &c, 1))
			;
		if (c != ':')
			return (0);
		while (c != '\n' && read(p, &c, 1))
		{
			if ((c < 32 || c > 127) && c != '\n')
				return (0);
		}
		a++;
	}
	close (p);
	return (1);
}
