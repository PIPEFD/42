/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/21 00:24:26 by dbonilla          #+#    #+#             */
/*   Updated: 2022/07/21 00:24:27 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(char *str)
{
	int	enter;
	int	number;

	enter = 1;
	number = 0;
	while (*str == 9 || *str == 10 || *str == 32 || *str == 11
		|| *str == 12 || *str == 13)
		str++;
	while (*str == 43 || *str == 45)
	{
		if (*str == '-')
			enter = enter * -1;
		str++;
	}
	while (*str >= 48 && *str <= 57)
	{
		number = (number * 10) + (*str - 48);
		str++;
	}
	return (number * enter);
}
