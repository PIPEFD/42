/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/06 12:08:32 by dbonilla          #+#    #+#             */
/*   Updated: 2022/07/13 11:17:21 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(int c)
{
	write (1, &c, 1);
}

void	ft_print(int a, int b)
{
	if (a < b)
	{	
		ft_putchar(48 + (a / 10));
		ft_putchar(48 + (a % 10));
		ft_putchar(32);
		ft_putchar(48 + (b / 10));
		ft_putchar(48 + (b % 10));
		if (a != 98 || b != 99)
		{
			ft_putchar(44);
			ft_putchar(32);
		}
	}
}

void	ft_print_comb2(void)
{
	int		a ;
	int		b ;

	a = 0;
	while (a <= 98)
	{
		b = 0;
		while (b <= 99)
		{
			ft_print(a, b);
			b++;
		}
		a++ ;
	}
}
