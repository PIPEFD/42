/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/27 16:49:58 by dbonilla          #+#    #+#             */
/*   Updated: 2022/09/29 19:12:54 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
// #include <stdio.h>
// #include <string.h>
// #include <stdlib.h>
char	*ft_strtrim(char const *s1, char const *set)
{
	char	*ptr;
	size_t	len;

	if (!s1 || !set)
		return (NULL);
	while (*s1 && ft_strchr(set, *s1))
		s1++;
	len = ft_strlen(s1);
	while (len && ft_strchr(set, s1[len]))
		len--;
	ptr = ft_substr(s1, 0, len + 1);
	if (!ptr)
		return (NULL);
	return (ptr);
}
// int main(void)
// {
//     char a[] = "hboblbab";
//     char b[] = "h";
//     printf("resultado: %s\n", ft_strtrim(a, b));
//     return(0);
// }