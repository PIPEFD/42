/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/23 18:17:56 by dbonilla          #+#    #+#             */
/*   Updated: 2022/09/29 19:46:24 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<string.h>
#include<stdio.h>
#include <stdlib.h>
#include"libft.h"

char	*ft_strjoin(const char *s1, const char *s2)
{
	size_t	i;
	size_t	j;
	char	*dst;

	i = 0;
	j = 0;
	if (s1 == NULL || s2 == NULL)
		return (NULL);
	dst = ft_calloc(sizeof(char), (ft_strlen(s1) + ft_strlen(s2) + 1));
	if (!dst)
		return (0);
	while (*(s1 + i))
	{
		*(dst + i) = *(s1 + i);
			i++;
	}
	while (*(s2 + j))
	{
			*(dst + i + j) = *(s2 + j);
			j++;
	}
	*(dst + i + j) = '\0';
	return (dst);
}

// int main (void)

// {
// 	char str1[] = "Hola Mundo";
// 	char str2[] = " Cruel";
// 	char  *str;
// 	str=ft_strjoin(str1, str2);

// 	printf("%s \n",str);
// 	return 0;	
// }