/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/31 16:31:25 by dbonilla          #+#    #+#             */
/*   Updated: 2022/09/21 19:11:41 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
// #include <stdio.h>
// #include <string.h>

size_t	ft_strlen(const char *s)
{
	unsigned int	count;

	count = 0;
	while (*s != '\0')
	{
		count++ ;
		s++;
	}
	return (count);
}

// int main (void)
// {
//  char str [10]= "hola mundo" ;
//  printf ("%lu \n", ft_strlen(str));
//  printf ("%lu", strlen(str));
// }