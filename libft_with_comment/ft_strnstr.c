/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/20 18:17:27 by dbonilla          #+#    #+#             */
/*   Updated: 2022/09/21 19:07:46 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// #include <string.h>
// #include <stdio.h>
#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	i;
	size_t	j;

	i = 0;
	if (needle[i] == '\0')
		return ((char *)haystack);
	while (haystack[i])
	{
			j = 0;
		while (haystack[i + j] == needle[j] && (i + j) < len)
		{
			if (needle[j + 1] == '\0')
				return ((char *)&haystack[i]);
				j++;
		}
		i++;
	}
	return (NULL);
}

// int main (void)
// {
// 	char *haystack = "Hola mundo";
// 	char *needle = "mundo" ;
// 	char *ptr;

// 	ptr = strnstr(haystack, needle, 12);
// 	printf ("%s", ptr);
// }