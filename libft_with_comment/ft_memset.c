/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/31 18:25:03 by dbonilla          #+#    #+#             */
/*   Updated: 2022/10/04 19:03:57 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Definicion*/
	/* */

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
#include "libft.h"
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero uno*/
	/* Llamado de la libreria local en donde se le indican 
		las librerias necesarias al momento 
		de realizar la compilación del script*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */

// #include <stdio.h>
// #include <string.h>

void	*ft_memset(void *b, int c, size_t len )
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero dos*/
	/* Declaracion de la funcion como " void " ya que no retorna ningun valor
	   En especifico, asi mismo se declara un puntero al nombre de la funcion
	   ya que es la direccion de memoria a donde apunta, las variables
	   necesarias para esta funcion son 3 para que pueda llevarse a cabo
	   la ejecución, el puntero de " *b " el cual se indica que es " Void "
	   sera el array a la que se remplazara con los valores declarados
	   en " c " y el tamaño o " Len " en el numero de posciones 
	   que se quiere realizar el cambio */

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
{
	unsigned char	*p;
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero tres*/
	/* Se declara " *p " el formato y variable como puntero o array en donde 
	   se almacenara los cambios realizados */

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */	

	p = b;
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero cuatro*/
	/* En p se alamcenara el string declarado previamente para realizar los 
	   cambios correspondientes */

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */	
	while (len > 0)
	{
		*p = c;
		p++;
		len--;
	}
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero cinco*/
	/*El " While " determinara que el numero de posciones 
	  a cambiar sea mayor a " 0 " En caso de cumplirse la condicion
	   l puntero " *p " tendra el cambio de " c "
	  en la poscion en la que se encuentre " Len "
	  una vez hecho el cambio el puntero de "p++" aumentara 
	  una poscion a lo que " Len-- " len decrementara para avazar a la siguiente
	  poscion y entrara de nuevo al bucle verificando si aun " Len " es mayor a 0*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */	
	return (b);
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero cinco*/
	/* Aunque no se espera un retorno de valor ya que la funcion incialmente
	   fue declarada como " void " , en esta caso retornara el array de " b "
	   el cual ya tendra los cambios hechos y los valores remplazados depues
	   de haber realizado la interaccion.*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */	
}
// int main (void) 
// {
// char str [10] = "Hola mundo" ;
// printf ("%s", ft_memset(str,'$', 1));
// return(0);
// }