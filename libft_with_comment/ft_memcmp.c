/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/12 18:07:08 by dbonilla          #+#    #+#             */
/*   Updated: 2022/09/21 18:58:49 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

// #include<string.h>
// #include<stdio.h>
int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char	*p;
	unsigned char	*c;

	p = (unsigned char *)s1 ;
	c = (unsigned char *)s2 ;
	while (n > 0)
	{
		if (*p != *c)
			return (*p - *c);
			p++ ;
			c++ ;
			n-- ;
	}
	return (0);
}

// int main (void)
// {
//  int ret ;
//  int res ;

//     //If s1 and s2 equal
//     ret = ft_memcmp("hola","hola",3);
//     res = memcmp("hola","hola",3);

//     printf("ret = %d\n",ret);
// 	printf("res = %d\n \n",res);
//     //if first char of s1 is large to s2
//     ret = ft_memcmp("ola","oola",3);
// 	res = memcmp("ola","oola",3);
//     printf("ret = %d\n",ret);
// 	printf("res = %d\n \n",res);

//     //if first char of s1 is small to s2
//     ret = ft_memcmp("holaA","hola",5);
//     res = memcmp("holaA","hola",5);
// 	printf("ret = %d\n",ret);
// 	printf("res = %d\n \n",res);

//     return 0;
// }