/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/13 18:33:52 by dbonilla          #+#    #+#             */
/*   Updated: 2022/10/04 15:17:39 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/* Definición */
/*La función ATOI o "Array To Integer" en su definicion es covertir una array 
	a un valor absoluto en un integer(int) para ello se debe realizar
	una serie de pasos necesarios para covertir dicha cadena
	para validar si el valor es negativo o postivo asi mismo si
   incluye caracter de por medio para avanzar en la cadena declarada */

/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero uno*/
/* Llamado de la libreria local en donde se le indican 
	las librerias necesarias al momento 
	de realizar la compilación del script*/
/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
#include "libft.h"
// #include <stdio.h>
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero dos*/

/* Declaración de la funcion el valor necesario
	 para poder ser verficiada, ademas de indicarle 
	que el valor devuelto sera de tipo int(integer).
	el valor que se necesitara sera un cadena
	 de tipo array "[]" o puntero "*" */

/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
int	ft_atoi(const char *str)
{
	int		i;
	int		sign;
	int		number;
/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */
/*Paso numero tres*/

/*	Declaracion de la variables y el tipo de formato en este caso 
	sera un "Int" integer de la misma en
	donde " i " sera el contador que recorrera el array, el valor " sign " 
	permite determinar el valor en negativo o postivo, 
	" number " es la variable donde almacenaremos el resultado de la operación
	donde convertiremos cada uno de los valores de los array " [] " 
	en integer "Int"
	*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */

	i = 0;
	sign = 1;
	number = 0;
/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */
/*Paso numero cuatro*/

 /* la variable " i " se establece en " 0 " de esta manera podremos 
 	recorrer el array " [] "desde la posicion " 0 " la variable 
	" sing " se establece en 1.	De esta manera en el caso que el valor 
	sea positivo se multiplique por si mismo
	o en el caso de ser negativo esta variable 
	se convertira mas adelante en un valor diferente.
	" number " se declara en 0 ya que sera necesaria para poder almacenar 
	el valor a ser devuelto depuesde hbaer realizado la operacion que permite 
	establecer el valor necesario para ser devuelto */

/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */


	while (str[i] == 9 || str[i] == 10 || str[i] == 11 || str[i] == 12
		|| str[i] == 13 || str[i] == 32)
		i++ ;
/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */

/*Paso numero cinco*/
/* Este " While " permite recorrer el string buscado los valores de la tabla ASCII
	como lo es espacio.comparando si son iguales en el caso de que sea alguno 
	de estos tenga espacios
	aumentara y continuara con el bucle hasta que se de 
	el caso que no sean iguales si no son iguales entrara 
	a la segunda condición de la funcion*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */
		
	if (str[i] == '+' || str[i] == '-' )
	{
		if (str[i] == '-' )
			sign = -1 ;
		i++;
	}
/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */
/*Paso numero seis*/
	/* El " if " dentro la funcion determinara si el valor es negativo o positivo
		si el valor es negativo entrara en la segunda condicion del " if "
		donde se determina si el valor es igual a negativo
		en caso de cumplir esta condición
		actualizara el valor de " sign " 
		 en donde se le indica que es igual a " -1 en el mismo caso " i++ " aumentaria */

/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */
	
	while (str[i] >= '0' && str[i] <= '9' )
	{
		number = (number * 10) + (str[i] - '0');
		i++;
	}
/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */
/*Paso numero siete*/
	/* En este caso el " While " le indica que en la poscion en la cual se encuentre el " Str " 
	debe ser mayor o igual (>=) " 0 " y (&&) menor o igual(<=) a " 9 "
	en el caso de cumplirse la condición.Se realizara la operacion aritmetica 
	en donde multiplicara el "number" " * " " 10 " mas el valor de " Str " en la posicion
	[i] - 0 para que de esta manera el valor sea constate y no cambie. (i++) 
	ira aumentando continuando con el recorrido de " Str " */
/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */

	return (number * sign);
/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */
/*Paso numero ocho*/
/*retorna el valor dado por el " While " en donde se establece o guarda en la variable " number "
de esta manera se multplica por la variable " sign " que en el caso que sea " - " 
deberia haber cambiado a " -1 "  se multiplicaran entre si estas dos variables para reto
rnar el valor de la función siendo un integer en cuestion, en el caso que sea " + "
 esta se multiplicara por el valor establecido en un principio el cual era " 1 " 
 dando el resultado en psotivo y retornado el valor solicitado */
	
/* ----  ------------  ---------- ----------- --------- --------- ---- ---- */

}

// int	main(void)
// {
// 	char	*nbr;

// 	nbr = " ------13d1";
// 	//printf("%d\n", atoi(nbr));	
// 	printf("%d\n", ft_atoi(nbr));	
// 	return(0);
// }