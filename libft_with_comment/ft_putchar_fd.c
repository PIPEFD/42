/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/28 15:36:36 by dbonilla          #+#    #+#             */
/*   Updated: 2022/09/29 18:56:17 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdio.h>

void	ft_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}

// int	main(void)
// {
// 	char a = 'H';
// 	char fd = 1;
// 	printf("BEFORE\n\tsrc: %c\n\tdes: %c\n", a, fd);
// 	ft_putchar_fd(a, fd);

// 	printf("AFTER\n\tsrc: %c\n\tdes: %c\n", a, fd);
// 	return (0);
// }
