/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/28 17:48:08 by dbonilla          #+#    #+#             */
/*   Updated: 2022/09/29 18:58:22 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// #include<stdio.h>
#include"stdio.h"

void	ft_striteri(char *s, void (*f)(unsigned int, char*))
{
	size_t	i;

	i = 0;
	if (!s || !f)
		return ;
	while (s[i])
	{
		f (i, &s[i]);
		i++;
	}
}

void	myfunc(unsigned int i, char *string)
{
	*string = *string + i;
}
// int main(void)
// {
// 	char str[] = "hola";
// 	printf ("string sin modificar; %s \n", str);
// 	ft_striteri(str, myfunc);
// 	printf ("string modificado; %s \n", str);
// 	return 0 ;
// }