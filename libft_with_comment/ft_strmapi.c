/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/29 17:42:44 by dbonilla          #+#    #+#             */
/*   Updated: 2022/10/04 18:06:30 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Definición*/
	/* Se aplica la funcion " f "  de la cadena " s " 
   lo cual crea una nueva cadena
   con " malloc " generando el resultado de 
   las sucesivas aplicacion de " f "*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
#include"libft.h"
// #include<stdio.h>
// #include<stdlib.h>
// #include<string.h>
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero uno*/
		/* Llamado de la libreria local en donde se le indican 
		las librerias necesarias al momento 
		de realizar la compilación del script*/
/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
char	*ft_strmapi(char *s, char (*f)(unsigned int, char))
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero dos*/
	/*Declaracion de la funcion con los valores necesarios 
	para poder realizar la ejecucion de la misma,
	la funcion tendra un char(character) como retorno de la misma
	en el caso " s " es donde se almacenara la cadena y en el " f " 
	sera la manera de llamar la funcion para poder  realizar las interaccion
	con cada uno de los valores dentro de " s "*/
/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
{
	unsigned int	i;
	char			*str;
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero tres*/
	/* Declaracion de  " i " como unsigned int del cual podremos dispones
	   para poder recorrer el array posicion a posicion , asi mismo un " *Str "
	   esta declaracion sera de tipo char (Character) el cual sera de tipo 
	   puntero y se creara la locacion dentro de la memoria con malloc */

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */

	if (!s)
		return (0);
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero cuatro*/
	/* En esta condición " If "
	   se intentara establecer si la cadena es diferente
	   avance al siguiente paso en el caso sea igual (==) 
	   retornara el valor de " 0 " */

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
	i = 0;
	str = (char *)malloc(sizeof(char) * (ft_strlen(s)) + 1);
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero cinco*/
	/* Se inicia la variable " i " en 0 que sera la posicion en la que 
	   empezara a recorrer, del mismo modo aqui se lleva a cabo la
	   asignacion de la memoria con malloc a tener en cuenta que el tamaño
	   del malloc esta condicionado por la funcion " ft_strlen "
	   la cual nos indicara el tamaño de la cadena " s " + " 1 " el
	   uno se para poder asignarle al final de la reserva el valor nulo */

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
	if (!str)
		return (0);
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero seis*/
	/* En este " If " se verifica que la cadena " Str " no este vacia 
	   de darse el caso en el que este vacia se retornara el valor de " 0 "*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
	while (s[i])
	{
		str[i] = f (i, s[i]);
		i++;
	}
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero siete */
	/* Dentro de este bucle " While " se llevara acabo 
	la pregunta de si es diferente o igual al valor indicado.
	mientras exista algun valor dentro de " S " en la poscion " i "
	se llevara a cabo la operacion y almacenamiento de nuevos valores
	dentro de " Str " teniendo en cuenta que primero se debe realizar 
	el cambio uno a uno en cada interaccion 
	de " i " para ello nos llevara a la funcion indicada que sera
	" my_func " dentro del mismo script, una vez terminado el bucle y 
	asignado los nuevos valores a " Str " */
/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
	str[i] = '\0';
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero diez*/
	/* Saldra del bucle y en la ultima poscion que marque " i " se indicara 
	que esta sea nula(NULL) */

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
	return (str);
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero once*/
	/* Se retornara el valor de " Str " alamacenada en una parte 
	   con los valores cambiados de acuerdo a los pasos anteriores
	   por los cuales debio pasar el string incial.*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
}	

char	my_func(unsigned int i, char str)
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero ocho*/
	/* Declaracion de la funcion como char(character)
		que generara los cambios dentro de la nueva 
		reserva de memoria " Str " de igual forma se le indicara el valor 
		de " i " el contador se matendra de acuerdo a la poscion en la cual 
		se encuentre ademas del " Str " para tener en cuenta el valor que se 
		debe cambiar*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
{

	if (i % 2 == 0)
		return (str - 32);
	else
		return (str);
/* ----  ------------  ---------- ----------- --------- --------- ---- ------ */
/*Paso numero nueve*/
	/* El " If "" establecera el cambio dentro de " Str " ya que de esta manera
	   se hara el cambio en el caso de que el resultado del modulo de " i " 
	   se igual a " 0 " de darse la condicion al valor que tiene " Str " en
	   ASCII le restara " - " " 32 " dando un nuevo valor a imprimir dentro 
	   de la tabla ASCII.
	   En el caso de que sea difetente a " 0 " la condicion else 
	   retornara de nuevo el valor original asignado en " Str ", 
	   esta interaccion se levara cabo con cada una de las posciones o del tamaño
	   que tenga la string original,retornado el valor nuevamente hacia 
	   funcion principal*/

/* ----  ------------  ---------- ----------- --------- --------- ---- ----- */
}

// int main (void)
// {
// 	char *str;
// 	char *res ;
// 	str = "!Hola Mundo¡" ;
// 	printf("Before strmapi src = %s\n", res);
// 	res = ft_strmapi(str, my_func);
// 	printf("After strmapi src = %s\n", res);
// 	return (0);

// }
