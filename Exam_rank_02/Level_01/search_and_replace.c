/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_and_replace.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/29 18:42:01 by dbonilla          #+#    #+#             */
/*   Updated: 2023/06/29 19:01:30 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	search_and_replace(char *str1, char *str2, char *str3)
{
	int	i;

	i = 0;
	if (!str2[1] && !str3[1])
	{
		while (str1[i])
		{
			if (str1[i] == str2[0])
				write(1, &str3[0], 1);
			else
				write(1, &str1[i], 1);
			i++;
		}
	}
}

int	main(int argc, char **argv)
{
	if (argc == 4)
	{
		search_and_replace(argv[1], argv[2], argv[3]);
	}
	write (1, "\n", 1);
}
