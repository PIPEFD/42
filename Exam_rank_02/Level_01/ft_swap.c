/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/28 12:22:26 by dbonilla          #+#    #+#             */
/*   Updated: 2023/06/30 17:27:33 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_swap(int *a, int *b)
{
	int	c;

	c = *a;
	*b = c;
}

int	main(void)
{
	int	i;
	int	j;
	int	*a = &i;
	int	*b = &j;

	i = 10;
	j = 9;
	ft_swap(a, b);
	printf("%d\n", *a);
	printf("%d", *b);
	return (0);
}
