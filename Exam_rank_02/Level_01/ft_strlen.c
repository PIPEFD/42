/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/30 15:47:16 by dbonilla          #+#    #+#             */
/*   Updated: 2023/06/30 17:21:52 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

size_t	ft_strlen(char *line)
{
	unsigned int	count;

	while (line != '\0')
	{
		count++;
		line++;
	}
	return (count);
}
