/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_print.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/30 10:55:50 by dbonilla          #+#    #+#             */
/*   Updated: 2023/06/30 17:27:56 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	rev_print(int argc, char *str)
{
	int	i;

	i = 0;
	if (argc == 2)
	{
		while (str[i])
		i++;
		while (i)
		write(1, &str[i], 1);
		i--;
	}
}

int	main (int argc,  char **argv)
{
	rev_print(argc, argv[1]);
}
// void	rev_print(int argc ,char *str)
// {
// 	int	i;

// 	i = 0;
// 	if (argc == 2)
// 	{
// 		while (str[i])
// 			i++;
// 		i--;
// 		while (str[i])
// 		{
// 			write(1, &str[i], 1);
// 			i--;
// 		}
		
// 	}
// }

