/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 11:55:20 by dbonilla          #+#    #+#             */
/*   Updated: 2023/06/28 16:33:58 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	ft_putstr(char *line)
{
	unsigned int	i;

	i = 0;
	if (line == NULL)
		return ('\0');
	while (line[i] != '\0')
	{
		write (1, &line[i], 1);
		i++;
	}
	return (0);
}

int	main(void)
{
	char	*str;

	str = "!HOLA MUNDO!";
	ft_putstr(str);
}
