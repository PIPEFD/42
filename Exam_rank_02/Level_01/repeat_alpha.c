/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repeat_alpha.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/27 17:32:54 by dbonilla          #+#    #+#             */
/*   Updated: 2023/06/27 18:38:21 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	main(int argc, char **argv)
{
	int	i;
	int	repeat;

	if (argv == NULL)
		return (0);
	if (argc == 2)
	{
		i = 0;
		while (argv[1][i])
		{
			repeat = 1;
			if (argv[1][i] >= 97 && argv[1][i] <= 122)
				repeat += argv[1][i] - 97;
			else if (argv[1][i] >= 65 && argv[1][i] <= 90)
				repeat += argv[1][i] - 65;
			while (repeat)
			{
				write(1, &argv[1][i], 1);
				repeat--;
			}
			i++;
		}
	}
	write(1, "\n", 1);
	return (0);
}
