/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/30 18:33:56 by dbonilla          #+#    #+#             */
/*   Updated: 2023/06/30 20:09:31 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(char *str)
{
	int	i;
	int	sing;
	int	number;

	i = 0;
	number = 0;
	sing = 1;
	if (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			sing = -1;
		i++;
	}
	while (str[i] && str[i] >= '0' && str[i] <= '9')
	{
		number = (number * 10) + (str[i] - '0');
		i++;
	}
	return (number * sing);
}

void	ft_print_hex(int number)
{
	char	*line;

	line = "0123456789abcdef";
	if (number > 16)
		ft_print_hex(number / 16);
	write (1, &line[number % 16], 1);
}

int	main(int argc, char **argv)
{
	if (argc == 2)
		ft_print_hex(ft_atoi(argv[1]));
}
