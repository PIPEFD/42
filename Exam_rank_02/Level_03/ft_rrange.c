/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rrange.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/27 19:06:47 by dbonilla          #+#    #+#             */
/*   Updated: 2023/06/28 12:18:07 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int	*ft_rrange(int start_line, int end_line)
{
	int	*range;
	int	i;
	int	size_line;

	i = 0;
	size_line = end_line - start_line + 1;
	if (end_line == '\0' || start_line == '\0')
		return (0);
	if (start_line > end_line)
		return (ft_rrange(end_line, start_line));
	range = (int *)malloc (sizeof (int) * size_line);
	if (range)
	{
		while (i < size_line)
		{
			range[i] = end_line;
			end_line--;
			i++;
		}
	}
	return (range);
}

int	main(void)
{
	int	i;
	int	*prt;
	int	num_1;
	int	num_2;

	num_1 = 1;
	num_2 = 3;
	i = 0;
	printf ("BEFORE -->(%i, %i) \n", num_1, num_2);
	prt = ft_rrange(num_1, num_2);
	while (i <= 2)
	{
		printf("AFTER -->%d ", prt[i]);
		i++;
	}
	printf("\n");
	return (0);
}
