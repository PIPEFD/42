/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   union.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/29 17:17:41 by dbonilla          #+#    #+#             */
/*   Updated: 2023/06/30 17:25:36 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void	ft_union(char *str1, char *str2)
{
	int	line[128] = {0};
	int	c;

	while (*str1 != '\0')
	{
		c = *str1;
		if (line[c] == 0)
		{
			write(1, str1, 1);
			line[c] = 9;
		}
		str1++;
	}
	while (*str2 != '\0')
	{
		c = *str2;
		if (line[c] == 0)
		{
			write(1, str2, 1);
			line[c] = 9;
		}
		str2++;	
	}
}

int	main(int argc, char **argv)
{
	if (argc == 3)
	{
		ft_union(argv[1], argv[2]);
	}
	write (1, "\n", 1);
}
