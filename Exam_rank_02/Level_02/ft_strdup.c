/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 12:35:05 by dbonilla          #+#    #+#             */
/*   Updated: 2023/07/04 15:15:18 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

size_t	ft_get_strlen(const char *line)
{
    unsigned int	count;
	
	count = 0;
	while (*line != '\0')
	{
		count++;
		line++;
	}
	return (count);
}

char	*ft_get_strdup(const char *line)
{
	int		i;
	char	*new_line;

	new_line = malloc (sizeof(char) * (ft_get_strlen(line) + 1));
	if (!new_line)
		return (NULL);
	i = 0;
	while (line[i] != '\0')
	{
		new_line[i] = line[i];
		i++;
	}
	new_line[i] = line[i];
	return (new_line);
}
