/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <dbonilla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/04 20:17:21 by dbonilla          #+#    #+#             */
/*   Updated: 2023/07/04 20:39:02 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int ft_atoi(char *line)
{
	int	i;
	int	sign;
	int	number;

	i = 0;
	number = 0;
	sign = 1;
	if (line[i] == '+' || line[i] == '-' )
		if (line[i] == '-')
			sign = -1;
	while (line[i] >= '0' && line[i] <= '9')
	{
		number = (number * 10) + (line[i] - '0');
		i++;
	}
	return (number * sign);
}

int main (void)
{
	printf(" AQUI -->> %i", ft_atoi("52"));
	return (0);
}
