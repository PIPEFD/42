/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zispizua <zispizua@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/02 09:22:56 by gode-la-          #+#    #+#             */
/*   Updated: 2022/07/02 19:07:27 by zispizua         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*Funcion que toma como argumentos el numero de filas (x) y columnas (y) e
imprime el valor correspondiente a la posicion.
BUCLE: Mientras i sea menor que nº filas
            posicion en columnas = 0
        Mientras j sea menor que nº columnas
            devuelveme el caracter que corresponde
            Imprimelo
            pasa a la siguiente columna
        pasa a la siguiente fila y a nueva fila*/

#include <unistd.h>

char	ft_putchar(int i, int j, int fila, int columna);

void	rush(int y, int x)
{
	int		i;
	int		j;
	char	c;

	i = 0;
	while (i < x)
	{
		j = 0;
		while (j < y)
		{
			c = ft_putchar(i, j, x, y);
			write (1, &c, 1);
			j++;
		}
		i++;
		write(1, "\n", 1);
	}
}

/*int main()
{
    rush(5,8);
    return (0);
}*/