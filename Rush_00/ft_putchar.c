/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zispizua <zispizua@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/01 22:07:37 by gode-la-          #+#    #+#             */
/*   Updated: 2022/07/02 19:03:57 by zispizua         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* El programa devuelve un caracter dependiendo de la posicion en la que 
estemos en el bucle. Tiene como argumentos 4 enteros que en orden son la
posicion en fila (i), posicion en columna (j), longitud de las filas y 
longitud de las columnas.
Construccion de condicional:
- Primera: Si es la 1ªfila y 1ªcol ó ultima fila y ultima col
- Segunda: Si es la 1ªfila y ultima col o ultima fila y 1ªcol
- Tercera: Si no es 1ªfila y si 1ªcol o si no es ultima fila y es ultima col
- Cuarta: Si es 1ªfila y no 1ªcol o si es ultima fila y no ultima col
- Quinta: los demas casos */

#include <unistd.h>

char	ft_putchar(int i, int j, int fila, int columna)
{
	if (((i == 0) && (j == 0)) || ((i == 0) && (j == columna - 1)))
	{
		return ('A');
	}
	else if (((i == fila - 1) && (j == columna - 1)) || ((i == fila - 1) && (j == 0)))
	{
		return ('C');
	}
	else if (((i != 0) && (j == 0)) || ((i != fila - 1) && (j == columna - 1)))
	{
		return ('B');
	}
	else if (((i == 0) && (j != 0)) || ((i == fila - 1) && (j != columna - 1)))
	{
		return ('B');
	}
	else
	{
		return (' ');
	}
}

/*int main ()
{
	int variable;
	variable = ft_putchar(4, 4, 5, 8);
	write(1, &variable, 1);
	return(0);
	}
*/