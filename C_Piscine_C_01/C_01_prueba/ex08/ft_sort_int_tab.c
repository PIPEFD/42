/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/14 12:17:33 by dbonilla          #+#    #+#             */
/*   Updated: 2022/07/14 12:17:35 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_sort_int_tab(int *tab, int size)
{
	int		i;
	int		mid ;

	i = 0;
	while (i < (size -1))
	{
		if (tab[i] > tab[i + 1])
		{
			mid = tab[i];
			tab[i] = tab[i + 1];
			tab[i + 1] = mid;
			i = 0;
		}
		else
		{
			i++;
		}
	}
}
