#include<stdio.h>

void	ft_rev_int_tab(int *tab, int size);

int		main (void)
{
	int tab[] = {1, 2, 3, 4};
	
    
printf("%d %d %d %d ", tab[0], tab[1],tab[2],tab[3]);
	ft_rev_int_tab(tab, 4);

	printf("Modified: ");
	printf("%d %d %d %d ", tab[0], tab[1],tab[2],tab[3]);
}