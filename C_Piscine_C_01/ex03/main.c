/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbonilla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/11 13:32:50 by dbonilla          #+#    #+#             */
/*   Updated: 2022/07/11 13:32:55 by dbonilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void  ft_div_mod( int a, int b, int *div, int *mod);




int main()
{
    int a;
    int *div;
    a = 20;
    div=&a;
     
    int b;
    int *mod;
    b= 5;
    mod=&b;
    

    
    ft_div_mod (a, b, div, mod);
    printf("%d \n",a);
    printf("%d \n",b);

    return (0);
}
